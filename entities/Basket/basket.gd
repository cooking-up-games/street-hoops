class_name Basket extends StaticBody3D

signal basket_made

@export var team : GameConstants.Team = GameConstants.Team.HOME

func _on_made_basket_area_body_entered(ball : Node3D) -> void:
	if is_instance_of(ball, Ball):
		basket_made.emit(team, 2)

