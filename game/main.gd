class_name Main extends Node3D

@onready var basket : Basket = %Basket

var home_team_score := 0
var away_team_score := 0

func _ready() -> void:
	basket.basket_made.connect(_basket_basket_made)

func _basket_basket_made(team : GameConstants.Team, points: int) -> void:
	match team:
		GameConstants.Team.HOME:
			home_team_score += points
		GameConstants.Team.AWAY:
			away_team_score += points

	update_score_ui()

func update_score_ui() -> void:
	print("Home: %s\nAway: %s\n===================" % [str(home_team_score), str(away_team_score)])
