extends CharacterBody3D

const SPEED = 25.0
const JUMP_VELOCITY = 12.5

@export var perfect_time := 0.5
@export var ball : Ball
@export var air_speed_modifier := 0.35

@onready var shoot_timer : Timer = %Timer
@onready var shoot_meter : HealthBar3D = %ShootMeterUI
@onready var ball_shoot_position : Marker3D = %BallShootPosition
@onready var player_animation : AnimationPlayer = %PlayerAnimationController

var shooting := false
var shooting_percent := 0.0
var speed = SPEED
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity") * 2.0

func _ready() -> void:
	player_animation.play(GameConstants.PLAYER_ANIM_DRIBBLE)

func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("shoot"):
		shoot_meter.visible = true
		shoot_timer.start()
		shooting = true

	if Input.is_action_just_released("shoot"):
		shoot_meter.visible = false
		shooting = false
		ball.shoot_ball(shooting_percent)

func _process(_delta: float) -> void:
	if shooting:
		ball.move_position(ball_shoot_position.global_position)
		shooting_percent = 1 - (shoot_timer.wait_time - shoot_timer.time_left) / perfect_time
		shoot_meter.value = (shoot_timer.wait_time - shoot_timer.time_left) / perfect_time

func _physics_process(delta):
	if not is_on_floor():
		velocity.y -= gravity * delta
		speed = SPEED * air_speed_modifier
	else:
		speed = SPEED


	if Input.is_action_just_pressed("shoot") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)

	move_and_slide()
