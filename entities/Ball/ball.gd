class_name Ball extends RigidBody3D

@onready var ball_mesh : Node3D = %BallMesh

@export var basket_marker : Marker3D
@export var fudge_amount := -0.05

var gravity_magnitude : float = ProjectSettings.get_setting("physics/3d/default_gravity")

var shot_from_position := Vector3.ZERO

func _ready() -> void:
	set_visible(false)

func get_shot_position() -> Vector3:
	return shot_from_position

func shoot_ball(shooting_percent: float) -> void:
	shot_from_position = global_position
	set_visible(true)
	gravity_scale = 2

	apply_impulse(calculate_initial_velocity(basket_marker.global_position, 50.0) * (mass + (fudge_amount * shooting_percent)))
	apply_torque_impulse(transform.basis.x * 2.0)

func move_position(pos : Vector3) -> void:
	linear_velocity = Vector3.ZERO
	angular_velocity = Vector3.ZERO
	PhysicsServer3D.body_set_state(
		get_rid(),
		PhysicsServer3D.BODY_STATE_TRANSFORM,
		Transform3D.IDENTITY.translated(pos)
	)
	ball_mesh.rotation.y = global_position.direction_to(basket_marker.global_position).y

func calculate_initial_velocity(target_position: Vector3, launch_angle: float) -> Vector3:
	var gravity := gravity_magnitude * gravity_scale
	var angle := deg_to_rad(launch_angle)
	var planar_target := Vector3(target_position.x, 0, target_position.z)
	var planar_position := Vector3(global_position.x, 0, global_position.z)
	var distance := planar_target.distance_to(planar_position)
	var y_offset := global_position.y - target_position.y
	var initial_velocity := (1 / cos(angle)) * sqrt((0.5 * gravity * pow(distance, 2)) / (distance * tan(angle) + y_offset))
	var velocity := Vector3(0, initial_velocity * sin(angle), initial_velocity * cos(angle))

	var modifier := -1.0

	if target_position.x > global_position.x:
		modifier = 1.0

	var angle_between_objects := global_transform.basis.z.angle_to(planar_target - planar_position) * modifier
	return Quaternion(global_transform.basis.y, angle_between_objects) * velocity;
