@tool

class_name HealthBar3D extends Node3D

var _value := 1.0
@export var value := 1.0 :
	get:
		return _value
	set(val):
		value_set(val)

var _full_color := Color.GREEN
@export var full_color : Color = Color.GREEN :
	get:
		return _full_color
	set(val):
		full_set(val)

var _empty_color := Color.RED
@export var empty_color : Color = Color.RED :
	get:
		return _empty_color
	set(val):
		empty_set(val)

var _outline_color := Color.BLACK
@export var outline_color : Color = Color.BLACK :
	get:
		return _outline_color
	set(val):
		outline_color_set(val)

var _size := Vector3(2,1,0.5)
@export var size := Vector3(2,1,0.5) :
	get:
		return _size
	set(val):
		size_set(val)

var _outline_size := 0.1
@export var outline_size := 0.1 :
	get:
		return _outline_size
	set(val):
		outline_size_set(val)

var _billboard_mode := 2
@export var billboard_mode := 2 :
	get:
		return _billboard_mode
	set(val):
		billboard_set(val)

var _unshaded := true
@export var unshaded := true :
	get:
		return _unshaded
	set(val):
		unshaded_set(val)

var progress:MeshInstance3D
var under:MeshInstance3D
var origin:Node3D
var progress_origin:Node3D

func _init():
	progress = MeshInstance3D.new()
	under = MeshInstance3D.new()
	origin = Node3D.new()
	progress_origin = Node3D.new()
	add_child(origin)
	origin.add_child(under)
	origin.add_child(progress_origin)
	progress_origin.add_child(progress)
	progress.mesh = BoxMesh.new()
	under.mesh = BoxMesh.new()
	progress.material_override = StandardMaterial3D.new()
	under.material_override = StandardMaterial3D.new()
	progress.material_override.billboard_keep_scale = true
	under.material_override.billboard_keep_scale = true
	
	outline_color_set(outline_color)
	size_set(size)
	value_set(value)
	billboard_set(billboard_mode)
	unshaded_set(unshaded)

func value_set(val):
	_value = val
	progress_origin.scale.y = _value
	progress.material_override.albedo_color = _empty_color.lerp(_full_color, _value)

func full_set(val):
	_full_color = val
	value_set(_value)

func empty_set(val):
	_empty_color = val
	value_set(_value)

func outline_color_set(val):
	_outline_color = val
	under.material_override.albedo_color = _outline_color

func size_set(val):
	_size = val
	progress.mesh.size = _size
	under.mesh.size.y = _size.y + _outline_size
	under.mesh.size.z = _size.z * 0.9
	under.mesh.size.x = _size.x + _outline_size

func outline_size_set(val):
	_outline_size = val
	size_set(_size)

func billboard_set(val):
	_billboard_mode = val
	match _billboard_mode:
		0:
			progress.material_override.billboard_mode = StandardMaterial3D.BILLBOARD_ENABLED
		1:
			progress.material_override.billboard_mode = StandardMaterial3D.BILLBOARD_FIXED_Y
		2:
			progress.material_override.billboard_mode = StandardMaterial3D.BILLBOARD_DISABLED
	under.material_override.billboard_mode = progress.material_override.billboard_mode

func unshaded_set(val):
	_unshaded = val
	progress.material_override.flags_unshaded = _unshaded
	under.material_override.flags_unshaded = _unshaded

