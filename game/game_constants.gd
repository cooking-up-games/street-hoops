class_name GameConstants extends Node

enum Team {
	HOME,
	AWAY
}

const PLAYER_ANIM_DRIBBLE := "Dribble"
